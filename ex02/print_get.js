const router = require('express').Router();

// All requests are from /ex02/...

router.get('/', (req, res) => {
    let text = "";
    for (const key in req.query) { 
        // text += key + ": " + req.query[key] + "</br>";
        text += `${key}: ${req.query[key]}</br>`;
    }
    res.send(text);
});

module.exports = router;