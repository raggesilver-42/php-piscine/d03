const express = require('express');
const app = express();
const path = require('path');

// //      rota      callback
// app.get('/ex01', (req, res) => {
//     res.send(process.versions);
// });

const cookieParser = require('cookie-parser');
const router = require('./router.js');

app.use('/ex04/', express.static(path.join(__dirname, 'ex04')));
// app.use('/img/', express.static(path.join(__dirname, 'img')));

app.use(cookieParser());
router(app);

app.listen(3000, () => {
    console.log("Server listening on port :3000");
});
