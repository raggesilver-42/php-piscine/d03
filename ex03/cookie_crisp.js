const router = require('express').Router();

// All requests are from /ex03/...

router.get('/', (req, res) => {
    
    if (!('action' in req.query))
        return res.status(400).send('No action specified.');
    if (!('name' in req.query))
        return res.status(400).send('No name specified.');
    if (req.query.action == 'set' && !('value' in req.query))
        return res.status(400).send('No value specified.');
    
    switch (req.query.action) {
        case 'set':
            res.cookie(req.query.name, req.query.value);
            break;
        case 'get':
            if (req.query.name in req.cookies)
                res.send(req.cookies[req.query.name]);
            break;
        case 'del':
            res.cookie(req.query.name, "", { maxAge: -1 });
            break;
        default:
            return res.status(400).send('Invalid action.');
    }
    
    res.end();
});

module.exports = router;