const router = require('express').Router();
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');

router.use(bodyParser.raw());

// All requests are from /ex06/...

let _401 = `<html><body>That area is accessible for members only</body></html>
`;

function refuse(res) {
    res.set('WWW-Authenticate', 'Basic realm="Member area"');
    return res.status(401).end(_401);
}

function guard(req, res, next) {
    if (!('authorization') in req.headers)
        return refuse(res);
    const b64auth = req.headers.authorization.split(' ')[1] || '';
    const [login, password] =
        Buffer.from(b64auth, 'base64').toString().split(':');
    if (login != 'zaz' || password != 'Ilovemylittleponey')
        return refuse(res);
    next();
}

function get_image_source() {
    let f = path.join(__dirname, '../img/42.png');
    let content = fs.readFileSync(f);
    return `data:image/png;base64,${Buffer.from(content).toString('base64')}`;
}

router.get('/', guard, (_req, res) => {
    res.send(`<html><body>
Hello Zaz<br />
<img src='${get_image_source()}'>
</body></html>
`);
});

module.exports = router;