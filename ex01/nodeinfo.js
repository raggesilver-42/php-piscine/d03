const router = require('express').Router();

// All requests are from /ex01/...

router.get('/', (req, res) => {
    res.send(process.versions);
});

module.exports = router;