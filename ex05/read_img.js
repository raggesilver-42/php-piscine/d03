const router = require('express').Router();
const path = require('path');

// All requests are from /ex05/...

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'img', '42.png'));
});

module.exports = router;